#!/usr/bin/env python3
import sys
import math
import numpy as np
import scipy.fftpack
from scipy import signal
import cv2
import time
from collections import deque
import pygame
from pygame.locals import *
from OpenGL.GL import *


import matplotlib.pyplot as plt
import numpy as np

plt.style.use('seaborn-poster')
#matplotlib inline

def reverseEnum(data):
    for i in range(len(data)-1, -1, -1):
        yield (i, data[i])

def fft(data):
    n = len(data)
    if n == 1:
        return data
    else:
        data_even = fft(data[ ::2])
        data_odd  = fft(data[1::2])
        factor = np.exp(-2j * np.pi * np.arange(n) / n)
        data = np.concatenate(
            [data_even + factor[:int(n/2)] * data_odd,
            data_even + factor[int(n/2)] * data_odd]
            )
    return data

#resolutions
#    [160, 120],
#    [192, 144],
#    [256, 144],
#    [240, 160],
#    [320, 240],
#    [360, 240],
#    [384, 240],
#    [400, 240],
#    [432, 240],
#    [480, 320],
#    [480, 360],
#    [640, 360],
#    [600, 480],
#    [640, 480],
#    [720, 480],


class PulseMonitorWebcam():
    def __init__(self, webcam = 0, history_len = 256, xcrop = 0.3, ycrop = 0.2, cam_width = 240,
                 cam_height = 160, fontsize = 16):
        self.history_len = history_len
        self.hData = deque([], self.history_len)
        self.hTime = deque([], self.history_len)
        self.hBpm  = deque([], self.history_len)
        self.timePrev = time.time()
        self.timeNow = time.time()
        self.frames = 0
        self.fps = 30
        self.xcrop = xcrop
        self.ycrop = ycrop
        self.fontsize = fontsize
        self.stableRect = None
        self.faceDetector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        self.cam = self.initCam(cam_width, cam_height, webcam)
        self.initOgl()
        
    def initOgl(self):
        pygame.init()
        pygame.font.init()
        self.font = pygame.font.SysFont('arial', self.fontsize)
        self.w = 800
        self.h = 600
        display = (self.w, self.h)
        pygame.display.set_mode(display, pygame.RESIZABLE | pygame.OPENGL | pygame.DOUBLEBUF)
        # pygame.DOUBLEBUF, pygame.NOFRAME ...
        pygame.display.set_caption('Pulse Monitor Webcam')
        glClearColor(1.0, 1.0, 1.0, 1.0)
        glLineWidth(3)
        glPointSize(4)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        glEnable( GL_LINE_SMOOTH )
        glEnable( GL_POLYGON_SMOOTH )
        glHint( GL_LINE_SMOOTH_HINT, GL_NICEST )
        glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST )
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        
    def mainloop(self):
        '''A pygame event loop which consumes and processes events from the pygame event queue'''
        keepGoing = True
        while keepGoing:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()
                    keepGoing = False
                    break
                elif event.type == pygame.KEYDOWN:
                    pass #keyDown(event.key)
                elif event.type == pygame.KEYUP:
                    pass
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    #mouseDown(event.pos, event.button)
                    pass
                elif event.type == pygame.VIDEORESIZE:
                    self.resize(event.size)
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            self.process()
            self.display()
            pygame.display.flip()
            pygame.time.wait(0)

    def resize(self, size):
        self.w, self.h = size
        print('Window resized to', size)
        glViewport(0, 0, GLsizei(self.w), GLsizei(self.h))
         
    def initCam(self, cam_width, cam_height, webcam = 0):
        '''
        Initialise the camera, and attempt to get a very small video resolution.
        '''
        cam = cv2.VideoCapture(webcam)
        success = True
        try:
            cam.set(cv2.CAP_PROP_FRAME_WIDTH, cam_width)
            cam.set(cv2.CAP_PROP_FRAME_HEIGHT, cam_height)
        except:
            success = False
        if success:
            print(f'Camera resolution set to {cam_width}x{cam_height}.')
        return cam

    def quit(self):
        self.cam.release()
        cv2.destroyAllWindows()
        pygame.quit()
        sys.exit()

    def detectFace(self, image):
        rects = self.faceDetector.detectMultiScale(
            image,
            scaleFactor = 1.1,
            minNeighbors = 5,
            minSize=(30, 30),
            flags = cv2.CASCADE_SCALE_IMAGE
        )
        if len(rects) > 0:
            return rects[0]
        return None
        
    def estimateHeartRate(self, data, samplingFreq):
        minHz = 30 / 60
        maxHz = 200 / 60
        sos = scipy.signal.iirdesign([.66, 3.0], [.5, 4.0], 1.0, 40.0, fs=samplingFreq, output='sos')
        #sos = scipy.signal.iirdesign(minHz / samplingFreq, maxHz / samplingFreq , 1.0, 40.0, fs=samplingFreq, output='sos')
        filteredData = scipy.signal.sosfiltfilt(sos, data)
        try:
            intensity = scipy.signal.detrend(filteredData)
        except:
            return 0
        freqs, pows = scipy.signal.welch(intensity, fs = samplingFreq, nperseg = 256)
        #self.hPows.append(pows)
        minIndex = 0
        maxIndex = len(freqs)
        for ii, e in enumerate(freqs):
            if e >= minHz:
                minIndex = ii
                break

        for ii, e in enumerate(freqs):
            if e > maxHz:
                maxIndex = ii
                break
        #p = [0] * len(pows)
        #for jj, p in enumerate(self.hPows):
        #    for ii, e in enumerate(p):
        #        try:
        #            p[ii] += e
        #        except:
        #            pass
        #for ii, e in enumerate(p):
            #p[ii] /= len(self.hPows)
        #bpm = round(freqs[np.argmax(p)] * 60, 2)
        #self.plot(freqs[minIndex:maxIndex], p[minIndex:maxIndex], 0.9, 0.1, 0.1, xmul = 60)
        return round(freqs[np.argmax(pows)] * 60, 2)

    def estimateInterpolatedHeartRate(self, data, times):
        '''
        Interpolation algorithm taken from:
        https://github.com/thearn/webcam-pulse-detector/blob/no_openmdao/lib/processors_noopenmdao.py
        '''
        processed = np.array(data)
        samples = processed
        l = len(data)
        if l <= 60:
            return 0
        outputDim = processed.shape[0]
        #we're operating on a deque, so -1 is the oldest, 0 is the newest
        fps = len(data) / (times[-1] - times[0])
        evenTimes = np.linspace(times[-1], times[0], l)
        interpolated = np.interp(evenTimes, times, processed)
        interpolated = np.hamming(l) * interpolated
        interpolated = interpolated - np.mean(interpolated)
        raw = np.fft.rfft(interpolated)
        self.fft = np.abs(raw)
        self.freqs = float(fps) / l * np.arange(l / 2 + 1)
        freqs = 60. * self.freqs
        idx = np.where( (freqs >= 50) & (freqs <= 200) )
        pruned = self.fft[idx]
        if len(pruned) == 0:
            return 0
        pfreq = freqs[idx]
        self.freqs = pfreq
        self.fft = pruned
        intensity = scipy.signal.detrend(self.fft)
        freqs, pows = scipy.signal.welch( self.fft, detrend = 'linear', fs = fps, nperseg = min( 256, len(intensity) ) )
        idx2 = np.argmax(intensity)
        self.bpm = self.freqs[idx2]
        self.plot(self.freqs, intensity, 0.9, 0.1, 0.1, xmul = 1)
        return self.bpm

    def updateFaceRect(self):
        rect = self.detectFace(self.frame)
        if rect is not None:
            if self.stableRect is None:
                self.stableRect = rect
                return
            if abs(self.stableRect[0] - rect[0]) >= self.stableRect[2] // 10 or abs(self.stableRect[1] - rect[1]) >= self.stableRect[3] // 10:
                    self.stableRect = rect
    
    def process(self):
        ret, self.frame = self.cam.read()
        if not ret:
            print("Failed to grab frame!")
            return
        self.timeNow = time.time()
        self.frames = self.frames + 1
        frameWidth = len(self.frame[0])
        frameHeight = len(self.frame)
        xStart = (frameWidth * self.xcrop) // 2
        xStop = frameWidth - xStart
        yStart = (frameHeight * self.ycrop) // 2
        yStop = frameHeight - yStart
        rect = None
        img = self.frame
        self.updateFaceRect()
        if self.stableRect is not None:
            x, y, w, h = self.stableRect
            h //= 4
            x += w // 4
            w = w // 2
            roi = self.frame[y:y+h, x:x+w]
            self.renderImage(roi, 0.89, 0.89, 0.1, 0.1, 1)
            img = roi
            
        pixels = 0
        redRoi = 0
        for jj, line in enumerate(img):
            for ii, pixel in enumerate(line):
                pixels += 1
                redRoi += pixel[2]#red is channel 2
        redRoi /= pixels

        pixels = 0
        redFrame = 0
        for jj, line in enumerate(self.frame):
            for ii, pixel in enumerate(line):
                pixels += 1
                redFrame += pixel[2]
        redFrame /= pixels
        red = redRoi - redFrame
        self.hData.append(red)
        self.hTime.append(self.timeNow)
        
        if self.timeNow - self.timePrev >= 1:
            self.fps = self.frames/(self.timeNow - self.timePrev)
            self.timePrev = self.timeNow
            self.frames = 0

    def mapVal(self, val, iStart, iStop, oStart, oStop):
        return oStart + (oStop - oStart) * (val - iStart) / (iStop - iStart)
            
    def renderImage(self, image, x = -1, y = -1, w = 2, h = 2, step = 1):
        frameWidth = len(image)
        frameHeight = len(image)
        glBegin(GL_POINTS)
        for jj, line in enumerate(image):
            if jj % step != 0:
                continue
            ypos = self.mapVal(frameHeight - jj, 0, frameHeight, y, y + h)
            for ii, pixel in enumerate(line):
                if ii % step != 0:
                    continue
                glColor(pixel[2] / 256, pixel[1] / 256, pixel[0] / 256, 1.0)
                glVertex(self.mapVal(ii, 0, frameWidth, x, x + w), ypos)
        glEnd()
            
    def getVectorRadius(self, vector):
        vmin = math.inf
        vmax = -math.inf
        for e in vector:
            if e < vmin:
                vmin = e
            if e > vmax:
                vmax = e
        radius = max( abs(vmin), abs(vmax) )
        return radius

    def getVectorMinMaxSpan(self, vector):
        vmin = math.inf
        vmax = -math.inf
        vmax = -math.inf
        for e in vector:
            if e < vmin:
                vmin = e
            if e > vmax:
                vmax = e
        return vmin, vmax, vmax - vmin
    
    def drawText(self, x, y, text):                                                
        textSurface = self.font.render(text, True, (255, 0, 0, 255), (255, 255, 255, 255))
        textData = pygame.image.tostring(textSurface, "RGBA", True)
        glWindowPos2d(x, y)
        glDrawPixels(textSurface.get_width(), textSurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData)

    def plot(self, x, y, r = 0.9, g = 0.1, b = 0.1, xmul = 1):
        xmin, xmax, xspan = self.getVectorMinMaxSpan(x)
        ymin, ymax, yspan = self.getVectorMinMaxSpan(y)
        xpos = 0
        ypos = 0
        glColor(r, g, b, 1.0)
        glBegin(GL_LINE_STRIP)
        for ii in range(len(x)):
            xpos = (x[ii] - xmin) / xspan * 1.8 - 0.9
            ypos = (y[ii] - ymin) / yspan * 1.8 - 0.9
            glVertex2d(xpos, ypos)
        glEnd()
        seqn = 0
        for ii in range( 0, len(x), max(1, len(x) // 15) ):
            xpos = (x[ii] - xmin) / xspan * 1.8 - 0.9
            ypos = 0
            if seqn % 2 != 0:
                ypos = self.fontsize // 2
            self.drawText( self.mapVal(xpos, -1, 1, 0, self.w), ypos, str( int(x[ii] * xmul) ) )
            seqn += 1
        
   
    def display(self):
        #dt = 1.0 / self.fps
        #t = np.arange(0, len(self.hData) * dt, dt)
        #y = scipy.fftpack.fft(self.hData)
        #N = len(y)
        #n = np.arange(N)
        #T = N / self.fps
        #freq = n / T
        #n_oneside = N // 2
        #f_oneside = freq[:n_oneside]
        #y_oneside = y[:n_oneside]/n_oneside
        #y_oneside = abs(y_oneside)
        ##filter frequencies between 40 bpm and 240 bpm
        #minHz = 40 / 60
        #maxHz = 240 / 60
        #minPos = 0
        #maxPos = 0
        #for ii, e in enumerate(f_oneside):
        #    if e >= minHz:
        #        minPos = ii
        #        break
        #for ii, e in enumerate(f_oneside):
        #    if e >= maxHz:
        #        maxPos = ii
        #        break
        #maxA = -math.inf
        #maxIndex = 0
        #for ii, e in enumerate(y_oneside[minPos:maxPos]):
        #    if e > maxA:
        #        maxA = e
        #        maxIndex = ii
        #maxIndex += minPos
        #self.plot(f_oneside[minPos:maxPos], y_oneside[minPos:maxPos])
        #cv2.line(img=self.frame, pt1=(10, 10), pt2=(100, 10), color=(255, 0, 0), thickness=5, lineType=8, shift=0)
        
        try:
            #estimate = self.estimateHeartRate(self.hData, self.fps)
            estimate = self.estimateInterpolatedHeartRate(self.hData, self.hTime)
        except:
            estimate = 0
        self.hBpm.append(estimate)
        print(f'Current bpm: {int(round(estimate, 0))}. Average bpm: {int(round(sum(self.hBpm) / len(self.hBpm), 0))}. FPS: {round(self.fps, 2)}.')




def main(argv):
    pmw = PulseMonitorWebcam(history_len = 1024)
    pmw.mainloop()


if __name__ == '__main__':
    main(sys.argv)
